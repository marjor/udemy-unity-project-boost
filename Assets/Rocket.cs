﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rocket : MonoBehaviour {

	Rigidbody rocketRigidbody;
	AudioSource audioSource;

	[SerializeField] float thrustPower = 30f;
	[SerializeField] float rotationSpeed = 200f;


	void OnCollisionEnter(Collision collision) {
		switch(collision.gameObject.tag) {
			case "Friendly":
				print("OK");
				break;
			case "Fuel":
				print("More fuel yay!");
				break;
			default:
				print("Dead");
				// kill player and restart level
				break;
		}
	}

	// Use this for initialization
	void Start () {
		rocketRigidbody = GetComponent<Rigidbody>();
		audioSource	= GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		HandleThrust();
		HandleRotation();
	}
	void HandleThrust() {
		if(Input.GetKey(KeyCode.Space)) {
			rocketRigidbody.AddRelativeForce(Vector3.up * this.thrustPower);
			PlayTrustSound();
			print("Adding thrust");
		} else {
			StopTrustSound();
		}
	}

	void HandleRotation() {
		this.rocketRigidbody.freezeRotation = true; // force manual control of rotation

		float rotationMultiplier = this.rotationSpeed * Time.deltaTime;
		if (Input.GetKey(KeyCode.LeftArrow) && !Input.GetKey(KeyCode.RightArrow)) {
			transform.Rotate(Vector3.forward * rotationMultiplier);
			print("Turninig right");
		}

		if (Input.GetKey(KeyCode.RightArrow) && !Input.GetKey(KeyCode.LeftArrow)) {
			transform.Rotate(Vector3.forward * (-rotationMultiplier));
			print("Turninig left");
		}

		this.rocketRigidbody.freezeRotation = false; // return rotation control to physics engine
	}

	void PlayTrustSound() {
		if(!audioSource.isPlaying) {
			audioSource.Play();
		}
	}

	void StopTrustSound() {
		if (audioSource.isPlaying) {
			audioSource.Stop();
		}
	}
	
}
